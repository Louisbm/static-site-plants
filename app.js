const express = require('express');

const app = express();

app.set('views', `${__dirname}/views`);
app.set('view engine', 'ejs');

app.use(express.static(`${__dirname}/public`));

app.get('/', (req, res) => {

    res.status(200).render('landing', {
        title: 'Green interior'
    });

});

const port = 3000;

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});