import { openNavbar, closeNavbar, closeNavbarOnLinkClicked, toggleStickyHeader, highlightActiveLink } from "./navbar.js";
import { toggleThemeBasedOnPreviousChoice, toggleIconThemeBasedOnPreviousChoice, getPreviousTheme, getPreviousIcon, toggleTheme } from "./theme.js";
import { configScrollReveal } from "./scrollRevealConfig.js";
import { toggleScrollUpButton } from "./scrollUpButton.js";
import { configAccordion } from "./accordion.js";

const navMenu = document.getElementById('nav-menu');
const navToggle = document.getElementById('nav-toggle');
const navClose = document.getElementById('nav-close');
const navLink = document.querySelectorAll('.nav__link');
const header = document.getElementById('header');

const sections = document.querySelectorAll('section[id]');

const scrollUpButton = document.getElementById('scroll-up');

const accordionItems = document.querySelectorAll('.questions__item');


openNavbar(navToggle, navMenu);
closeNavbar(navClose, navMenu);

navLink.forEach((linkElement) => { closeNavbarOnLinkClicked(linkElement, navMenu); });

window.addEventListener('scroll', () => { toggleStickyHeader(header); });


/* ACCORDION */
configAccordion(accordionItems);

/* SCROLL SECTIONS ACTIVE LINK */
window.addEventListener('scroll', () => { highlightActiveLink(sections) ; });

/* SCROLL UP BUTTON */
window.addEventListener('scroll', () => { toggleScrollUpButton(scrollUpButton) });

/* DARK/LIGHT THEME */
const themeButton = document.getElementById('theme-button');

const selectedTheme = getPreviousTheme();
const selectedIcon = getPreviousIcon();

if (selectedTheme) {
  toggleThemeBasedOnPreviousChoice(selectedTheme);
  toggleIconThemeBasedOnPreviousChoice(themeButton, selectedIcon);
}

toggleTheme(themeButton);

/* SCROLL REVEAL JS */
configScrollReveal();

