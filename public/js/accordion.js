/**
 * Toggle an accordion.
 * 
 * @param {HTMLElement} accordionItem Accordion
 */
const toggleItem = (accordionItem) => {
    const accordionContent = accordionItem.querySelector('.questions__content')

    if (accordionItem.classList.contains('accordion-open')) {

        accordionContent.removeAttribute('style')
        accordionItem.classList.remove('accordion-open')

    } else {

        accordionContent.style.height = accordionContent.scrollHeight + 'px'
        accordionItem.classList.add('accordion-open')

    }
};

/**
 * Dispatch event on accordion items.
 * 
 * @param {NodeList} accordionItems Accordion elements list
 */
export const configAccordion = (accordionItems) => {

    accordionItems.forEach((item) =>{

        const accordionHeader = item.querySelector('.questions__header')
    
        accordionHeader.addEventListener('click', () =>{
            const openItem = document.querySelector('.accordion-open')
    
            toggleItem(item);
    
            if (openItem && openItem!== item) {
                toggleItem(openItem);
            }
        });

    });

};